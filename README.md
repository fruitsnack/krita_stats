# krita_stats

A script to generate statistics on krita folder projects.
Outputs total time spent editing, total amount of files, average editing time,
longest edited file and some other useful stuff.

## Requirements

- unzip
- xmllint

## Usage

Consult script's --help output for usage.  
Warning: be careful running this script in your ~ or /, as it might take
a long time to scan your entire FS.

Example:  
`krita_stats ~/krita_projects`

## License
This script is licensed under coffeeware (fork of beerware) license.

#!/bin/bash

#----------------------------------------------------------------------------
# "THE COFFEE-WARE LICENSE" (based on the beer-ware license revision 42):
# Fruitsnack wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a coffee in return.
#----------------------------------------------------------------------------

xml_path="*[name()='document-info']/*[name()='about']/\
*[name()='editing-time']/text()"

if ! which unzip xmllint > /dev/null 2>&1; then 
    "Can't find unzip or xmllint. Both binaries are required in PATH."
    exit 127
fi

function ExtractTime(){
    unzip -p "$1" documentinfo.xml | xmllint --xpath "${xml_path}" - 2>&-
}

function PrintHelp(){
    echo "Usage:"
    echo "--no-longest, -l: don't display longest edited file stats."
    echo "--no-total, -t: don't display total time stats."
    echo "--no-average, -a: don't display average time stats."
    echo "--help, -h: display this text."
    echo "Any other argument will be treated as path to look for kra files in."
    echo "Only one search path is supported."
}

search_path="$(pwd)"
longest=1
total=1
average=1

while (( $# )); do
    case $1 in
        --no-longest|-l)
            unset longest
        ;;
        --no-total|-t)
            unset total
        ;;
        --no-average|-a)
            unset average
        ;;
        --help|-h)
            PrintHelp
            exit 0
        ;;
        *)
            if [[ -z $path_set ]]; then
                search_path="$1"
                path_set=1
            else
                echo "Unrecognized argument $1."
                PrintHelp
                exit 1
            fi
        ;;
    esac
    shift
done

if [[ -z $path_set ]]; then
    echo "Search path is not specified."
    PrintHelp
    exit 1
fi

file_list=$(find "$search_path" -iname "*.kra")
total_files=0
total_time=0
biggest_time=0
biggest_file=""

IFS=$'\n'

for i in $file_list; do
    current_time=$(ExtractTime "$i")
    if [[ -n $current_time && $current_time =~ ^[0-9]+$ ]]; then
        total_time=$(( $total_time+ $current_time))
        total_files=$(( $total_files+ 1 ))
        if (( $current_time > $biggest_time )); then
            biggest_time=$current_time
            biggest_file="${i}"
        fi
    fi
done

if [[ $total_files -eq 0 ]]; then
    echo "No krita files found."
    exit 0
fi

function ConvertTime() {
    minutes=$(( $1 / 60 ))
    hours=$(( $minutes / 60 ))
    days=$(( hours / 24 ))
    hour_remainder=$(( $hours % 24 ))
    minute_remainder=$(( $minutes % 60 ))
}

if [[ -n $total ]]; then
    ConvertTime $total_time
    echo "Total editing time: ${days} days, ${hour_remainder} hours, \
${minute_remainder} minutes."
    echo "${hours} hours in total, ${minutes} minutes in total."
    echo "Total amount of kra files: ${total_files}."
fi

if [[ -n $longest ]]; then
    ConvertTime $biggest_time
    echo "Longest edited file: ${biggest_file}."
    echo "Editing time: ${hours} hours, ${minute_remainder} minutes."
fi

if [[ -n $average ]]; then
    ConvertTime $(( $total_time / $total_files ))
    echo "Average editing time: ${hours} hours, ${minute_remainder} minutes."
fi
